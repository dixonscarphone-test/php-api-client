<?php
namespace DcApi\RestClient\Tests;

use DcApi\RestClient\DcEventsClient;
use GuzzleHttp\Command\ResultInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Tests\Server;

/**
 * @coversDefaultClass \DcApi\RestClient\DcEventsClient
 */
class DcEventsClientTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DcEventsClient
     */
    public $client;
    protected function setUp(): void
    {
        parent::setUp();
        Server::start();
        register_shutdown_function(function(){Server::stop();});
        $this->client = DcEventsClient::create([
            'base_url' => Server::$url,
            'config_file' => 'events.json'
        ]);
    }
    /**
     * @covers ::create
     */
    public function testFactory() {
        self::assertInstanceOf(DcEventsClient::class, $this->client);
    }


    public function testSend() {
        $data = [
            'event' => [
                "id" => '2016.20',
                "type" => 2,
                "timestamp" => '1460110095'
            ]
        ];
        Server::enqueue([new Response(200, [], json_encode(['status' => 'OK','payload' => $data], TRUE))]);
        $response = $this->client->Send($data);
        self::assertInstanceOf(ResultInterface::class, $response);
        self::assertEquals($data, $response->toArray()['payload']);
    }
}
