<?php

namespace DcApi\RestClient\Tests;

use DcApi\RestClient\DcLogsClient;
use GuzzleHttp\Command\ResultInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Tests\Server;

/**
 * @coversDefaultClass \DcApi\RestClient\DcLogsClient
 */
class DcLogsClientTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DcLogsClient
     */
    public $client;
    protected function setUp(): void
    {
        parent::setUp();
        Server::start();
        register_shutdown_function(function(){Server::stop();});
        $this->client = DcLogsClient::create([
            'base_url' => Server::$url,
            'config_file' => 'logs.json'
        ]);
    }
    /**
     * @covers ::create
     */
    public function testFactory() {
        self::assertInstanceOf(DcLogsClient::class, $this->client);
    }


    public function testSend() {
        $data = [
            'what' => 'DEPLOY-2016.20',
            'tags' => 'code-release',
            'datetime' => '2016-07-08 10:08:15'
        ];
        Server::enqueue([new Response(200, [], json_encode(['status' => 'OK','payload' => $data], TRUE))]);
        $response = $this->client->Send($data);
        self::assertInstanceOf(ResultInterface::class, $response);
        self::assertEquals($data, $response->toArray()['payload']);
    }
}
