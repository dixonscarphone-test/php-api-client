FROM php:7.2-cli-alpine

RUN apk update && apk add --no-cache  \
    libxml2-dev \
    && docker-php-ext-install mbstring \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install dom \
    && docker-php-ext-install sockets \
    && rm -rf /tmp/*

WORKDIR /app

COPY . /app

RUN apk add --no-cache git \
    && curl -sS https://getcomposer.org/installer | php \
    && ./composer.phar install --no-dev --no-interaction \
    && rm ./composer.phar \
    && apk del git

CMD [ "php", "./src/DcApp.php" ]