# PHP API Client

This application was built using [Guzzle Services](https://github.com/guzzle/guzzle-services) to describe API points and communication with them.
This application is intended to be run in Docker environment but can be launched from elsewhere if the required environment variables are defined.

## Prerequisites

Docker and Docker-Compose need to be installed.

## Configuration

Create an ```.env``` file and define MQ connection parameters:

```
MQ_HOST
MQ_PORT
MQ_USER
MQ_PASS
MQ_VHOST
```

## Usage

```docker-compose up -d --build```