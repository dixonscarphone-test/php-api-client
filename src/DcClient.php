<?php

namespace DcApi\RestClient;

use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\Description;

abstract class DcClient extends GuzzleClient
{
    public static function create($config = []) {
        $service_description = new Description(
            ['baseUrl' => $config['base_url']] + (array) json_decode(file_get_contents(__DIR__ . '/../' . $config['config_file']), TRUE)
        );
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);
        return new static($client, $service_description, NULL, NULL, NULL, $config);
    }

    public abstract function sendRequest($params = []);
}