<?php

namespace DcApi\RestClient;
use \Datetime;


class DcEventsClient extends DcClient
{
    public function sendRequest($params = [])
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $params["datetime"]);
        $timestamp = $date->getTimestamp();
        $arguments = [
            'event' => [
                "id" => $params["releaseNum"],
                "type" => 2,
                "timestamp" => $timestamp
            ]
        ];
        $command = $this->getCommand('Send', $arguments);
        // mock request result
        return (bool)random_int(0, 1);
        /*$result_object = $this->execute($command);
        $result_array = $result_object->toArray();
        if (!$result_array['success']) {
            return false;
        }
        else return true;*/
    }
}