<?php
/**
 * Created by PhpStorm.
 * User: eshkigal
 * Date: 17.02.19
 * Time: 11:23
 */

namespace DcApi\RestClient;

class DcLogsClient extends DcClient {

    public function sendRequest($params = [])
    {
        $arguments = [
            'what' => 'DEPLOY-' . $params["releaseNum"],
            'tags' => 'code-release',
            'datetime' => $params["datetime"]
        ];
        $command = $this->getCommand('Send', $arguments);
        // mock request result
        return (bool)random_int(0, 1);
        /*$result_object = $this->execute($command);
        $result_array = $result_object->toArray();
        if (!$result_array['success']) {
            return false;
        }
        else return true;*/
    }
}