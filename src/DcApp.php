<?php

namespace DcApi\RestClient;

require_once __DIR__ . '/../vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class DcApp
{
    private $api_clients;

    public function run() {
        $dc_logs_client = DcLogsClient::create([
            'base_url' => 'http://logs.dixonscarphonetest.com',
            'config_file' => 'logs.json'
        ]);
        $dc_events_client = DcEventsClient::create([
            'base_url' => 'http://events.dixonscarphonetest.com',
            'config_file' => 'events.json'
        ]);
        $this->api_clients = array("events" => $dc_events_client, "logs" => $dc_logs_client);

        $connection = new AMQPStreamConnection(getenv('MQ_HOST'), getenv('MQ_PORT'), getenv('MQ_USER'), getenv('MQ_PASS'), getenv('MQ_VHOST'));
        $channel = $connection->channel();

        $channel->queue_declare('citomonitor', false, false, false, false);

        echo " [*] Waiting for messages.\n";

        $callback = function ($msg) {
            echo ' [x] Received ', $msg->body, "\n";
            $params = json_decode($msg->body, true);
            $api_client = $this->api_clients[$params['target']];
            if (!$api_client->sendRequest($params)) {
                $message = new AMQPMessage($msg->body);
                $msg->delivery_info['channel']->basic_publish($message, '', 'citomonitor');
            }
        };

        $channel->basic_consume('citomonitor', '', false, true, false, false, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }
}

$app = new DcApp();
$app->run();